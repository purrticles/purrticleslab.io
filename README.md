# [Purrticles](https://purrticles.com) by [Vince Varga, the Serial Developer](https://serialdeveloper.com)

> **Kitten-sized programming articles.**
>
> Learn the Basics of any Technology or Concept *in 25 minutes*.

## Articles

### Published Articles

### In Progress

1. HTML in 25 minutes

### Planned

## Instructions

### New Book

1. For writing new books, clone [purrticles/base-article](https://gitlab.com/purrticles/base-article) and create a new repository for the article
2. Add article title to the "In Progress" list and remove from the "Planned" list
3. *Write the article!*
4. Move from the "In Progress" list to the "Published Articles" list. Make sure the repository is public.

The one-repo-per-article process is set up so it's easier to keep articles up-to-date and to accept user feedback, open issues and accept pull requests.

[Group Avatar Image by Octavio Fossatti from Unsplash](https://unsplash.com/photos/B5tUnCpJw6s)
